
AWSTemplateFormatVersion: 2010-09-09
Description: >-
  AWS CloudFormation task and service defintions with "task-ervice auto scaling"

Parameters:
  InfraStackName:
    Type: String
    Description: Name of infra Stack CloudFormation
  LogGroupName:
    Type: String
    Default: arbitrage-group
    Description: Name of Log Group
  ApiContainerInfos:
    Type: CommaDelimitedList
    Default: api-container,3000
    Description: 'ContainerName and ContainerPort for each container launched by task service (api) Format: containername,8888'
  FrontContainerInfos:
    Type: CommaDelimitedList
    Default: front-container,4200
    Description: 'ContainerName and ContainerPort for each container launched by task service (api) Format: containername,8888'


Resources:
#Log Group
  LogGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Ref LogGroupName
      RetentionInDays: 5

#Task définitions
  TaskApi:
    Type: AWS::ECS::TaskDefinition
    Properties:
      Family: api-filtrage
      NetworkMode: bridge
      Cpu: '300'
      Memory: '450'
      ExecutionRoleArn: !GetAtt RoleECSTaskExecution.Arn
      ContainerDefinitions:
        - Name: !Select [ 0, !Ref ApiContainerInfos ]
          Image: enzo13/api-arbitrage-p:3.0.1
          Cpu: 300
          Memory: 450
          MemoryReservation: 270
          PortMappings:
            - HostPort: 0
              Protocol: tcp
              ContainerPort: 3000
          Command:
            - npm
            - run
            - start
          Environment:
            - Name: API_NAME
              Value: api1
            - Name: API_PORT
              Value: '3000'
            - Name: MONGO_DB
              Value: arbitrage_db
            - Name: MONGO_HOSTNAME
              Value: mongo.enzo-cora.com
            - Name: MONGO_INITDB_USERNAME
              Value: api
            - Name: MONGO_INITDB_PASSWORD
              Value: apipwd
            - Name: MONGO_PORT
              Value: '27017'
            - Name: NODE_ENV
              Value: production
          WorkingDirectory: "/usr/src/app"
          HealthCheck:
            Retries: 2
            Command:
              - CMD-SHELL
              - curl -f http://127.0.0.1:3000/api1/test || exit 1
            Timeout: 2
            Interval: 30
            StartPeriod: 10
          Essential: true
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group: !Ref LogGroup
              awslogs-region: !Ref "AWS::Region"
              awslogs-stream-prefix: api
      RequiresCompatibilities:
        - EC2

  TaskFront:
    Type: AWS::ECS::TaskDefinition
    Properties:
      Family: front-adminPanel
      NetworkMode: bridge
      Cpu: '300'
      Memory: '350'
      ExecutionRoleArn: !GetAtt RoleECSTaskExecution.Arn
      ContainerDefinitions:
        - Name: !Select [ 0, !Ref FrontContainerInfos ]
          Image: enzo13/front-arbitrage-p:3.0
          Cpu: 300
          Memory: 350
          MemoryReservation: 250
          PortMappings:
            - HostPort: 0
              Protocol: tcp
              ContainerPort: 4200
          WorkingDirectory: "/etc/nginx/conf.d"
          Command:
            - /bin/bash
            - -c
            - envsubst '$SITE_HOSTNAME $SITE_PORT'< /etc/nginx/conf.d/server_web.conf.template > /etc/nginx/conf.d/server_web.conf && nginx -g 'daemon off;'
          Environment:
            - Name: SITE_HOSTNAME
              Value: front-arbitrage-p
            - Name: SITE_PORT
              Value: 4200
          HealthCheck:
            Retries: 3
            Command:
              - CMD-SHELL
              - curl -f http://127.0.0.1:4200 || exit 1
            Timeout: 3
            Interval: 30
            StartPeriod: 15
          Essential: true
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group: !Ref LogGroup
              awslogs-region: !Ref "AWS::Region"
              awslogs-stream-prefix: front
      RequiresCompatibilities:
        - EC2

#ServicesTask
  ServiceTaskApi:
    Type: AWS::ECS::Service
    Properties:
      ServiceName: api-service
      Cluster:
        Fn::ImportValue: !Sub "${InfraStackName}-cluster-name"
      TaskDefinition: !Ref TaskApi
      DesiredCount: 1
      SchedulingStrategy: REPLICA
      CapacityProviderStrategy:
        - CapacityProvider:
            Fn::ImportValue: !Sub "${InfraStackName}-capacity-provider-default-name"
          Weight: 1
      PlacementStrategies:
        - Field: memory
          Type: binpack
      DeploymentConfiguration:
        DeploymentCircuitBreaker:
          Enable: true
          Rollback: false
        MaximumPercent: 200
        MinimumHealthyPercent: 100
      DeploymentController:
        Type: ECS
      LoadBalancers:
        - ContainerName: !Select [ 0, !Ref ApiContainerInfos ]
          ContainerPort: !Select [ 1, !Ref ApiContainerInfos ]
          TargetGroupArn:
            Fn::ImportValue: !Sub "${InfraStackName}-target-group-api-arn"
      HealthCheckGracePeriodSeconds: 30
      Role: !Sub 'arn:aws:iam::${AWS::AccountId}:role/aws-service-role/ecs.amazonaws.com/AWSServiceRoleForECS'

  ServiceTaskFront:
    Type: AWS::ECS::Service
    Properties:
      ServiceName: front-service
      Cluster:
        Fn::ImportValue: !Sub "${InfraStackName}-cluster-name"
      TaskDefinition: !Ref TaskFront
      DesiredCount: 1
      SchedulingStrategy: REPLICA
      CapacityProviderStrategy:
        - CapacityProvider:
            Fn::ImportValue: !Sub "${InfraStackName}-capacity-provider-default-name"
          Weight: 1
      PlacementStrategies:
        - Field: memory
          Type: binpack
      DeploymentConfiguration:
        DeploymentCircuitBreaker:
          Enable: true
          Rollback: false
        MaximumPercent: 200
        MinimumHealthyPercent: 100
      DeploymentController:
        Type: ECS
      LoadBalancers:
        - ContainerName: !Select [ 0, !Ref FrontContainerInfos ]
          ContainerPort: !Select [ 1, !Ref FrontContainerInfos ]
          TargetGroupArn:
            Fn::ImportValue: !Sub "${InfraStackName}-target-group-front-arn"
      HealthCheckGracePeriodSeconds: 30
      Role: !Sub 'arn:aws:iam::${AWS::AccountId}:role/aws-service-role/ecs.amazonaws.com/AWSServiceRoleForECS'

#Targets Application AutoScaling
  ApplicationScalingTargetServiceApi:
    Type: AWS::ApplicationAutoScaling::ScalableTarget
    DependsOn: ServiceTaskApi
    Properties:
      MaxCapacity: 3
      MinCapacity: 0
      ResourceId: !Join #service/clusterName/ServiceName
          - '/'
          - - 'service'
            - Fn::ImportValue: !Sub "${InfraStackName}-cluster-name"
            - !GetAtt ServiceTaskApi.Name
      RoleARN: !Sub 'arn:aws:iam::${AWS::AccountId}:role/aws-service-role/ecs.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_ECSService'
      ScalableDimension: ecs:service:DesiredCount
      ServiceNamespace: ecs

  ApplicationScalingTargetServiceFront:
    Type: AWS::ApplicationAutoScaling::ScalableTarget
    DependsOn: ServiceTaskFront
    Properties:
      MaxCapacity: 3
      MinCapacity: 0
      ResourceId: !Join #service/clusterName/ServiceName
        - '/'
        - - 'service'
          - Fn::ImportValue: !Sub "${InfraStackName}-cluster-name"
          - !GetAtt ServiceTaskFront.Name
      RoleARN: !Sub 'arn:aws:iam::${AWS::AccountId}:role/aws-service-role/ecs.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_ECSService'
      ScalableDimension: ecs:service:DesiredCount
      ServiceNamespace: ecs

#Policies Application AutoScaling
  ApplicationScalingPolicyServiceApi:
    Type: AWS::ApplicationAutoScaling::ScalingPolicy
    Properties:
      ScalingTargetId: !Ref ApplicationScalingTargetServiceApi
      PolicyName: cpu-autoscaling-policy
      PolicyType: TargetTrackingScaling
      TargetTrackingScalingPolicyConfiguration:
        DisableScaleIn: false
        PredefinedMetricSpecification:
          PredefinedMetricType: ECSServiceAverageCPUUtilization
        ScaleInCooldown: 60
        ScaleOutCooldown: 60
        TargetValue: 120

  ApplicationScalingPolicyServiceFront:
    Type: AWS::ApplicationAutoScaling::ScalingPolicy
    Properties:
      ScalingTargetId: !Ref ApplicationScalingTargetServiceFront
      PolicyName: cpu-autoscaling-policy
      PolicyType: TargetTrackingScaling
      TargetTrackingScalingPolicyConfiguration:
        DisableScaleIn: false
        PredefinedMetricSpecification:
          PredefinedMetricType: ECSServiceAverageCPUUtilization
        ScaleInCooldown: 60
        ScaleOutCooldown: 60
        TargetValue: 120


#Role IAM
  RoleECSTaskExecution:
    Type: AWS::IAM::Role
    Properties:
      RoleName: Role-ECSTaskExecution
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ecs-tasks.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      Description: Provides access to other AWS service resources that are required to run Amazon ECS tasks
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy
